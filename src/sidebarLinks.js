export default [
  {
    name: 'Overview',
    icon: 'nc-icon nc-layout-11',
    path: '/overview'
  },
  {
    name: 'Users',
    icon: 'nc-icon nc-circle-10',
    path: '/users'
  },
  {
    name: 'Sales Reps',
    icon: 'nc-icon nc-single-02',
    path: '/reps'
  },
  {
    name: 'Offices',
    icon: 'nc-icon nc-briefcase-24',
    path: '/office'
  },
  {
    name: 'Reports',
    icon: 'nc-icon nc-paper',
    path: '/reports'
  }
]
