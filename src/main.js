import Vue from 'vue'
import './pollyfills'
import VueRouter from 'vue-router'
import VueNotify from 'vue-notifyjs'
import VeeValidate from 'vee-validate'
import lang from 'element-ui/lib/locale/lang/en'
import locale from 'element-ui/lib/locale'
import App from './App.vue'

// Plugins
import GlobalComponents from './globalComponents'
import GlobalDirectives from './globalDirectives'
import SideBar from './components/UIComponents/SidebarPlugin'
import initProgress from './progressbar';

// library imports
import './assets/sass/paper-dashboard.scss'
import './assets/sass/element_variables.scss'
import './assets/sass/demo.scss'
import sidebarLinks from './sidebarLinks'

const package_json = require('../package.json');
const version = package_json.version;
Vue.prototype.$version = version;

//global plugin for auth
// import authPlugin from './authPlugin';
// Vue.use(authPlugin);
Vue.use(VueRouter);
Vue.use(GlobalDirectives);
Vue.use(GlobalComponents);
Vue.use(VueNotify);
Vue.use(SideBar, {sidebarLinks: sidebarLinks});
Vue.use(VeeValidate);
locale.use(lang);

//axios
import axios from "axios";
import VueAxios from "vue-axios";
//axios.defaults.baseURL = 'https://us-central1-admin-dev-82b6b.cloudfunctions.net/api';
axios.defaults.baseURL = 'http://localhost:5000';
console.log("axios base url " + axios.defaults.baseURL);
Vue.use(VueAxios, axios);

//router
import routes from './routes/routes'
const router = new VueRouter({
  routes, // short for routes: ro
  // utes
  linkActiveClass: 'active'
});
import auth from './firebaseConfig.js';
router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(x => x.meta.requiresAuth);
  const currentUser = auth().currentUser;
  //console.log(currentUser);
  if (requiresAuth && !currentUser) {
    next('/login')
  } else if (requiresAuth && currentUser) {
    next()
  } else {
    next()
  }
});
initProgress(router);

//store
import { store } from './store.js'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store
});

//handle page reloads
let app;
auth().onAuthStateChanged(user => {
  if (!app) {
    app = new Vue({
      el: '#app',
      router,
      store,
      render: h => h(App)
    })
  }
});
