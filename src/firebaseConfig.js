import firebase from 'firebase';

// Initialize Firebase
let config = {
  apiKey: "AIzaSyBDhMb-6xG5u08r2rgFCJ9_IkDv8DJ0wew",
  authDomain: "admin-dev-82b6b.firebaseapp.com",
  databaseURL: "https://admin-dev-82b6b.firebaseio.com",
  projectId: "admin-dev-82b6b",
  storageBucket: "admin-dev-82b6b.appspot.com",
  messagingSenderId: "872600958244"
};
firebase.initializeApp(config);
const auth = firebase.auth;

export default auth;
