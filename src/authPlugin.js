import auth from './firebaseConfig.js';
let authPlugin = {};
authPlugin.install = function (Vue, options) {
  Vue.prototype.$auth = auth;
};

export default authPlugin;
