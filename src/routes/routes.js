import DashboardLayout from '../components/Dashboard/Layout/DashboardLayout.vue'
// GeneralViews
import NotFound from '../components/GeneralViews/NotFoundPage.vue'
// Dashboard pages
const Overview = () => import(/* webpackChunkName: "overview" */ 'src/components/Dashboard/Views/Dashboard/Overview.vue')
const Reports = () => import(/* webpackChunkName: "reports" */ 'src/components/Dashboard/Views/Dashboard/Reports.vue')

// Pages
import Login from 'src/components/Dashboard/Views/Pages/Login.vue';
import Register from 'src/components/Dashboard/Views/Pages/Register.vue'

const UserList = () => import(/* webpackChunkName: "userlist" */ 'src/components/UserAdmin/UserList.vue');
const RepList = () => import(/* webpackChunkName: "replist" */ 'src/components/UserAdmin/RepList.vue');
const OfficeList = () => import(/* webpackChunkName: "officelist" */ 'src/components/UserAdmin/OfficeList.vue');

const routes = [
  {
    name: "Overview",
    path: '/',
    component: DashboardLayout,
    children: [
      {
        path: 'overview',
        name: 'Overview',
        component: Overview,
        meta: {
          requiresAuth: true
        }
      }],
    meta: {
      requiresAuth: true
    }
  },
  {
    name: "Users",
    path: '/users',
    component: DashboardLayout,
    redirect: '/users/list',
    children: [
      {
        path: 'list',
        name: 'Users',
        component: UserList,
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/reps',
    component: DashboardLayout,
    redirect: '/reps/list',
    children: [
      {
        path: 'list',
        name: 'Reps',
        component: RepList,
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/office',
    component: DashboardLayout,
    redirect: '/office/list',
    children: [
      {
        path: 'list',
        name: 'Offices',
        component: OfficeList,
        meta: {
          requiresAuth: true
        }
      }
    ]
  },
  {
    path: '/',
    component: DashboardLayout,
    children: [
      {
        path: 'reports',
        name: 'Reports',
        component: Reports,
        meta: {
          requiresAuth: true
        }

      }]
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  },
  {
    path: '/register',
    name: 'Register',
    component: Register
  },
  {path: '*', component: NotFound}
];

export default routes
